import { Select } from 'antd';
import { Fragment } from 'react';
import 'antd/dist/antd.css';

const Filter = ({onChange, style, name}) =>
{
    return (
    <Fragment>
    <span class="ant-input-group-addon" style={{width: "8%", verticalAlign: ".125em"}}>{name}</span>
    <Select
                mode="tags"
                onChange={onChange}
                style={style}
                tokenSeparators={[',']}>
            </Select>
            </Fragment>);
}

export default Filter;