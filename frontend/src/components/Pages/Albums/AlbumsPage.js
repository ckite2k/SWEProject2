import React, { useState, useEffect, Fragment, useRef } from "react"
import { Pagination, Input, Select, Slider } from "antd"
import { Button } from "react-bootstrap"
import "antd/dist/antd.css"
import NavBar from "../../NavBar"
import Filter from "../../Filter"
import AlbumCard from "../../Cards/AlbumCard"
const https = require("https")
const { Search } = Input
const { Option } = Select
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"

function AlbumsPage() {
	const [loading, setLoading] = useState(true)
	const [albumData, setAlbumData] = useState([])
	const [total, setTotal] = useState(20)
	const [page, setPage] = useState(1)
	const [perPage, setPerPage] = useState(20)
	const [search, setSearch] = useState("")
	const [artist, setArtist] = useState([])
	const [genres, setGenres] = useState([])
	const [popularity, setPopularity] = useState([])
	const [tracks, setTracks] = useState([])
	const [sortBy, setSortBy] = useState("name")
	const [sortDir, setSortDir] = useState(0)
	const [onPage, setOnPage] = useState(12)
	const [compare1, setCompare1] = useState("")
	const [compare1ID, setCompare1ID] = useState(0)
	const [compare2, setCompare2] = useState("")
	const [compare2ID, setCompare2ID] = useState(0)
	const albumRef = useRef(null)

	const pageChange = (page, perPage) => {
		setPage(page)
		setPerPage(perPage)
		console.log(page)
		console.log(perPage)
	}

	useEffect(() => {
		//Make API call with parameters for current page and per page to get the data for this page

		const fetchAlbums = async () => {
			try {
				setLoading(true)
				const httpsAgent = new https.Agent({
					rejectUnauthorized: false,
				})
				const requestOptions = {
					method: "GET",
					agent: httpsAgent,
					headers: {},
				}

				var link = "https://spotifynder.com/api/albums?"
				link += "page="
				link += page
				link += "&perPage="
				link += perPage
				if (search !== "" && search !== undefined) link += "&search="
				link += search
				if (artist.length !== 0) {
					artist.forEach(function (a) {
						link += "&artist="
						link += a
					})
				}
				if (genres.length !== 0) {
					genres.forEach(function (g) {
						link += "&genres="
						link += g
					})
				}
				if (popularity.length !== 0) {
					popularity.forEach(function (a) {
						link += "&popularity="
						link += a
					})
				}
				if (tracks.length !== 0) {
					tracks.forEach(function (a) {
						link += "&totalTracks="
						link += a
					})
				}
				link += "&sort="
				link += sortBy
				link += "&direction="
				link += sortDir
				console.log(link)
				const response = await fetch(link, requestOptions)
				const data = await response.json()
				console.log("fetched")
				setAlbumData(data.message)
				setTotal(data.count)
				setOnPage(perPage)
				if (page - 1 === total / perPage) {
					setOnPage(total % perPage)
				}

				setLoading(false)
			} catch (error) {
				console.error(error)
			}
		}
		fetchAlbums()
	}, [page, perPage, search, artist, genres, popularity, tracks, sortBy, sortDir, total])
	
	const handleCallback = (name, albumID) => {
		if(compare1 === name)
		{
			setCompare1("")
			setCompare1ID("");
			return false;
		}
		else if(compare2 === name)
		{
			setCompare2("")
			setCompare2ID("");
			document.getElementById("CompareButton").style.visibility = "hidden"
			return false;
		}
		else if(compare1 !== "" && compare2 !== "")
			return false
		else if(compare1 === "")
		{
			setCompare1(name)
			setCompare1ID(albumID);
			return true;
		}
		else if(compare2 === "")
		{
			setCompare2(name)
			setCompare2ID(albumID);
			document.getElementById("CompareButton").style.visibility = "visible"
			return true;
		}
	}

	return (
		<Fragment>
			<NavBar />
			<h1 style={{ marginTop: "25px", textAlign: "center" }}>Albums</h1>

			<div class="search-bar" style={{ display: "flex", margin: "auto", justifyContent: "center", width: "85%" }}>
				<Search addonBefore="Album" placeholder="search.." onSearch={setSearch} />
			</div>

			<div class="filter-bar">
				<Filter name={"Artist"} onChange={setArtist} style={{ width: "100%" }}></Filter>
				<Filter name={"Genre"} onChange={setGenres} style={{ width: "100%" }}></Filter>

				<Select onSelect={setSortBy} defaultValue="name" style={{ width: "10%" }}>
					<Option value="name">Name</Option>
					<Option value="artist">Artist</Option>
					<Option value="genres">Genre</Option>
					<Option value="popularity">Popularity</Option>
					<Option value="totalTracks">Tracks</Option>
				</Select>
				<Select onSelect={setSortDir} defaultValue="0" style={{ width: "10%" }}>
					<Option value="0">Ascending</Option>
					<Option value="1">Descending</Option>
				</Select>
			</div>

			<div class="filter-bar">
				<div class="filter-box-2">
					<span style={{ display: "inline-block" }}>Popularity</span>
					<Slider range defaultValue={[0, 100]} onAfterChange={setPopularity} />
				</div>
				<div class="filter-box-2">
					<span style={{ display: "inline-block" }}># of Tracks</span>
					<Slider range defaultValue={[0, 200]} max="200" onAfterChange={setTracks} />
				</div>
			</div>


			<div style={{ margin: "auto", textAlign: "center", fontFamily: "Arial, Helveticca, sans-serif"}}>
				<h4 style={{marginBottom: 5}}>Select two albums from the grid to compare.</h4>
				<h4>Comparing {compare1 + (compare1 !== "" && compare2 !== "" ? " and " : "") + compare2}</h4>
				<Button id="CompareButton" href={`/albums/compare/${compare1ID}/${compare2ID}`} style={{ visibility: "hidden", marginRight: 2, marginTop: 2 }}>Go!</Button>
			</div>


			{!loading ? (
				<div style={{ width: "85%", margin: "auto", marginTop: 50 }}>
					<section style={{ display: "grid", gridTemplateRows: "repeat(auto-fill, minmax(10px, 1fr))", gridTemplateColumns: "repeat(auto-fill, minmax(270px, 1fr))", gap: "50px" }} ref={albumRef}>
						{albumData?.map((data) => (
							<AlbumCard data={data} search={search} callback={handleCallback} key={data.id} />
						))}
					</section>
				</div>
			) : (
				<div>Loading...</div>
			)}
			<Pagination
				total={total}
				pageSize={perPage}
				onChange={pageChange}
				defaultCurrent={1}
				style={{
					margin: "16px 0",
					display: "flex",
					justifyContent: "center",
				}}
				showTotal={(total) => `Showing ${onPage} of ${total} items`}
			/>
		</Fragment>
		// <Pagination />
	)
}

export default AlbumsPage
