import React, { useState, useEffect } from "react"
import { Link, useParams } from "react-router-dom"
import NavBar from "../../NavBar"
const https = require("https")
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"

function AlbumComparePage() {
	const { album1ID, album2ID } = useParams()
	const [album1Data, setAlbum1Data] = useState({})
	const [album2Data, setAlbum2Data] = useState({})

	const getAlbumData = async () => {
		try {
			const httpsAgent = new https.Agent({
				rejectUnauthorized: false,
			})
			const requestOptions = {
				method: "GET",
				agent: httpsAgent,
				headers: {},
			}
			const response1 = await fetch(`https://spotifynder.com/api/albums?albumID=${album1ID}`, requestOptions)
			const data1 = await response1.json()
			setAlbum1Data(data1.message[0])
			
			const response2 = await fetch(`https://spotifynder.com/api/albums?albumID=${album2ID}`, requestOptions)
			const data2 = await response2.json()
			setAlbum2Data(data2.message[0])
		} catch (e) {
			console.error(e)
		}
	}

	useEffect(() => {
		getAlbumData()
	})

	return (
		<div>
			<div>
				<NavBar />
			</div>
			<a href="/Albums" class="btn btn-primary" style={{ marginTop: "15px", marginLeft: "15px" }}>
				Back to Albums
			</a>
			<div style={{ margin: "auto", marginTop: "50px", width: "80%" }}>
				<div class="jumbotron">
					<div>
						<h3 class="display-3" style={{ display: "inline-block", width: "75%" }}>
							{album1Data.name}
						</h3>
						<img src={album1Data.albumImage} alt={album1Data.name} style={{ float: "right", width: "250px" }} />
						<h1 class="display-4" style={{ marginBottom: "150px" }}>
							Artist:{" "}
							<Link key={album1ID} to={`/artist/${album1Data.artistID}`}>
								{album1Data.artist}
							</Link>
						</h1>
					</div>
					<hr class="my-4"></hr>
					<div id="body">
						<div style={{ display: "inline-block" }}>
							<h3>
								<strong>Genre</strong>: {album1Data.genres === "" ? "Unknown" : album1Data.genres}
							</h3>
							<h3>
								<strong>Released</strong>: {album1Data.releaseDate}
							</h3>
							<h3>
								<strong>Tracks</strong>: {album1Data.totalTracks}
							</h3>
							<h3>
								<strong>Studio</strong>: {album1Data.label}
							</h3>
							<h3>
								<strong>Recommended Song</strong>:{" "}
								<Link key={album1ID} to={`/song/${album1Data.recommendedSongID}`}>
									{album1Data.recommendedSong}
								</Link>
							</h3>
						</div>
						<iframe title="Album 1 preview" src={`https://open.spotify.com/embed/album/${album1Data.albumID}`} width="250" height="250" frameborder="0" allowtransparency="true" allow="encrypted-media" style={{ float: "right" }}></iframe>
					</div>
				</div>
			</div>


			<div style={{ margin: "auto", marginTop: "50px", width: "80%" }}>
				<div class="jumbotron">
					<div>
						<h3 class="display-3" style={{ display: "inline-block", width: "75%" }}>
							{album2Data.name}
						</h3>
						<img src={album2Data.albumImage} alt={album2Data.name} style={{ float: "right", width: "250px" }} />
						<h1 class="display-4" style={{ marginBottom: "150px" }}>
							Artist:{" "}
							<Link key={album2ID} to={`/artist/${album2Data.artistID}`}>
								{album2Data.artist}
							</Link>
						</h1>
					</div>
					<hr class="my-4"></hr>
					<div id="body">
						<div style={{ display: "inline-block" }}>
							<h3>
								<strong>Genre</strong>: {album2Data.genres === "" ? "Unknown" : album2Data.genres}
							</h3>
							<h3>
								<strong>Released</strong>: {album2Data.releaseDate}
							</h3>
							<h3>
								<strong>Tracks</strong>: {album2Data.totalTracks}
							</h3>
							<h3>
								<strong>Studio</strong>: {album2Data.label}
							</h3>
							<h3>
								<strong>Recommended Song</strong>:{" "}
								<Link key={album2ID} to={`/song/${album2Data.recommendedSongID}`}>
									{album2Data.recommendedSong}
								</Link>
							</h3>
						</div>
						<iframe title="Album 2 preview" src={`https://open.spotify.com/embed/album/${album2Data.albumID}`} width="250" height="250" frameborder="0" allowtransparency="true" allow="encrypted-media" style={{ float: "right" }}></iframe>
					</div>
				</div>
			</div>

			<br/>
		</div>
	)
}

export default AlbumComparePage
