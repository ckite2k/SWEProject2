import React, { useState, useEffect, Fragment, useRef } from "react"
import { Pagination, Input, Select, Slider } from "antd"
import "antd/dist/antd.css"
import NavBar from "../../NavBar"
import Filter from "../../Filter"
import ArtistSearchCard from "../../Cards/ArtistSearchCard"
const https = require("https")
const { Search } = Input
const { Option } = Select
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"

function ArtistSearchPage() {
	const [loading, setLoading] = useState(true)
	const [artistData, setArtistData] = useState([])
	const [total, setTotal] = useState(20)
	const [page, setPage] = useState(1)
	const [perPage, setPerPage] = useState(20)
	const [search, setSearch] = useState("")
	const [genres, setGenres] = useState([])
	const [popularity, setPopularity] = useState([])
	const [intFormedYear, setIntFormedYear] = useState([])
	const [intMembers, setIntMembers] = useState([])
	const [sortBy, setSortBy] = useState("name")
	const [sortDir, setSortDir] = useState(0)
	const [onPage, setOnPage] = useState(12)
	const artistRef = useRef(null)

	const pageChange = (page, perPage) => {
		setPage(page)
		setPerPage(perPage)
		console.log(page)
		console.log(perPage)
	}

	useEffect(() => {
		//Make API call with parameters for current page and per page to get the data for this page

		const fetchArtists = async () => {
			try {
				setLoading(true)
				const httpsAgent = new https.Agent({
					rejectUnauthorized: false,
				})
				const requestOptions = {
					method: "GET",
					agent: httpsAgent,
					headers: {},
				}
				var link = `https://spotifynder.com/api/artists?`
				link += "page="
				link += page
				link += "&perPage="
				link += perPage
				if (search !== "" && search !== undefined) link += "&search="
				link += search
				if (genres.length !== 0) {
					genres.forEach(function (a) {
						link += "&strGenre="
						link += a
					})
				}
				if (popularity.length !== 0) {
					popularity.forEach(function (g) {
						link += "&popularity="
						link += g
					})
				}
				if (intFormedYear.length !== 0) {
					intFormedYear.forEach(function (a) {
						link += "&intFormedYear="
						link += a
					})
				}
				if (intMembers.length !== 0) {
					intMembers.forEach(function (a) {
						link += "&intMembers="
						link += a
					})
				}
				link += "&sort="
				link += sortBy
				link += "&direction="
				link += sortDir
				console.log(link)
				const response = await fetch(link, requestOptions)
				const data = await response.json()
				console.log("fetched")
				let artistArray = data.message
				for (let artist of artistArray) {
					if (artist.intFormedYear + "" === "-1" || artist.intFormedYear + "" === "0") {
						artist.intFormedYear = "Unknown"
						artist.strDisbanded = "Unknown"
					}
					if (artist.intMembers + "" === "-1") {
						artist.intMembers = "Unknown"
					}
					if (artist.strGenre === "") {
						artist.strGenre = "Unknown"
					}
				}

				setArtistData(artistArray)
				setTotal(data.count)
				setOnPage(perPage)
				if (page - 1 === total / perPage) {
					setOnPage(total % perPage)
				}

				setLoading(false)
			} catch (error) {
				console.error(error)
			}
		}
		fetchArtists()
	}, [page, perPage, search, genres, popularity, intFormedYear, intMembers, sortBy, sortDir, total])

	return (
		<Fragment>
			<NavBar />

			<h1 style={{ marginTop: "25px", textAlign: "center" }}>Search Artists</h1>

			<div
				class="search-bar"
				style={{
					display: "flex",
					margin: "auto",
					justifyContent: "center",
					width: "85%",
				}}>
				<Search addonBefore="Search" placeholder="search.." onSearch={setSearch} />
			</div>

			<div class="filter-bar">
				<Filter name={"Genre"} onChange={setGenres} style={{ width: "100%" }}></Filter>
				<Select onSelect={setSortBy} defaultValue="name" style={{ width: "10%" }}>
					<Option value="name">Name</Option>
					<Option value="strGenre">Genre</Option>
					<Option value="popularity">Popularity</Option>
					<Option value="intFormedYear">Year Formed</Option>
					<Option value="intMembers">Members</Option>
				</Select>
				<Select onSelect={setSortDir} defaultValue="0" style={{ width: "10%" }}>
					<Option value="0">Ascending</Option>
					<Option value="1">Descending</Option>
				</Select>
			</div>

			<div class="filter-bar">
				<div class="filter-box-2">
					<span style={{ display: "inline-block" }}>Popularity</span>
					<Slider range defaultValue={[0, 100]} onAfterChange={setPopularity} />
				</div>
				<div class="filter-box-2">
					<span style={{ display: "inline-block" }}>Year Formed</span>
					<Slider range defaultValue={[1900, 2021]} min={1900} max={2021} onAfterChange={setIntFormedYear} />
				</div>
				<div class="filter-box-2">
					<span style={{ display: "inline-block" }}>Number of Members</span>
					<Slider range defaultValue={[0, 10]} max={10} onAfterChange={setIntMembers} />
				</div>
			</div>

			{!loading ? (
				<div style={{ width: "85%", margin: "auto", marginTop: 50 }}>
					<section
						style={{
							display: "grid",
							gridTemplateRows: "repeat(auto-fill, minmax(300px, 1fr))",
							gridTemplateColumns: "repeat(auto-fill, minmax(400px, 1fr))",
							gap: "50px",
						}}
						ref={artistRef}>
						{artistData?.map((data) => (
							<ArtistSearchCard artist={data} search={search} key={data.id} />
						))}
					</section>
				</div>
			) : (
				<div>Loading...</div>
			)}
			<Pagination
				total={total}
				pageSize={perPage}
				onChange={pageChange}
				defaultCurrent={1}
				style={{
					margin: "16px 0",
					display: "flex",
					justifyContent: "center",
				}}
				showTotal={(total) => `Showing ${onPage} of ${total} items`}
			/>
		</Fragment>
	)
}

export default ArtistSearchPage
