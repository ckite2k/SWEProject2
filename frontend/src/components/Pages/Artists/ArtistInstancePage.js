import React, { useEffect, useState } from "react"
import { useParams } from "react-router-dom"

import NavBar from "../../NavBar"
const https = require("https")
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"

function ArtistInstancePage() {
	const { artistID } = useParams()
	const [artistData, setArtistData] = useState({})

	const getArtistData = async () => {
		try {
			const httpsAgent = new https.Agent({
				rejectUnauthorized: false,
			})
			const requestOptions = {
				method: "GET",
				agent: httpsAgent,
				headers: {},
			}
			const response = await fetch(`https://spotifynder.com/api/artists?artistID=${artistID}`, requestOptions)
			console.log(response)
			const data = await response.json()
			let artist = data.message[0]
			if (artist.intFormedYear + "" === "-1" || artist.intFormedYear + "" === "0") {
				artist.intFormedYear = "Unknown"
				artist.strDisbanded = "Unknown"
			}
			if (artist.intMembers + "" === "-1") {
				artist.intMembers = "Unknown"
			}
			if (artist.strGenre === "") {
				artist.strGenre = "Unknown"
			}
			setArtistData(artist)
			console.log(artistData)
		} catch (e) {
			console.error(e)
		}
	}

	useEffect(() => {
		getArtistData()
	})

	function addCommas(nStr) {
		nStr += ""
		let x = nStr.split(".")
		let x1 = x[0]
		let x2 = x.length > 1 ? "." + x[1] : ""
		let rgx = /(\d+)(\d{3})/
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, "$1,$2")
		}
		return x1 + x2
	}
	function twitterExists() {
		return (artistData.strTwitter + "").length > 3
	}
	const songLink = `/song/${artistData.topID}`
	const twitHandle = (artistData.strTwitter + "").substring("twitter.com/".length)
	const twitSource = `data:text/html;charset=utf-8,%3Ca%20class%3D%22twitter-timeline%22%20href%3D%22https%3A//${artistData.strTwitter}%3Fref_src%3Dtwsrc%255Etfw%22%3ETweets%20by%20${twitHandle}%3C/a%3E%0A%3Cscript%20async%20src%3D%22https%3A//platform.twitter.com/widgets.js%22%20charset%3D%22utf-8%22%3E%3C/script%3E%0A`

	const albumLink = `/album/${artistData.topAlbumID}`

	return (
		<div>
			<NavBar />
			<a href="/artists" class="btn btn-primary" style={{ marginTop: "15px", marginLeft: "15px" }}>
				Back to Artists
			</a>
			<div className="container">
				<div className="row justify-content-around">
					<div className="col-md-9">
						<div class="header" style={{ margin: "auto", display: "flex", justifyContent: "center" }}>
							<div class="card mb-3" style={{ width: "1000px" }}>
								<h2 class="card-header">{artistData.name}</h2>
								<div class="card-body">
									<h5 class="card-title">Followers: {addCommas(artistData.followers)}</h5>
								</div>
								<img class="artist-image" src={artistData.strArtistThumb} alt="" />
								<div class="card-body">
									<div className="container">
										<div className="row justify-content-around">
											<div className="col-md-6">
												<p class="card-text">
													Active Years: {artistData.intFormedYear} - {artistData.strDisbanded}
												</p>
											</div>
											<div className="col-md-6">
												<p class="card-text">Genre: {artistData.strGenre}</p>
											</div>
										</div>
										<br />
										<div className="row justify-content-around">
											<div className="col-md-6">
												<p class="card-text">Members: {artistData.intMembers}</p>
											</div>
											<div className="col-md-6">{artistData.strCountry !== "" && <p class="card-text">From: {artistData.strCountry}</p>}</div>
										</div>
										<br />
										<div className="row justify-content-around">
											<div className="col-md-6">
												<p class="card-text">
													Top Track:{" "}
													<a href={songLink} id="top-track" class="card-link">
														{artistData.topTrack}
													</a>
												</p>
											</div>
											<div className="col-md-6">
												<p class="card-text">
													Top Album:{" "}
													<a href={albumLink} id="top-album" class="card-link">
														{artistData.topAlbum}
													</a>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="col-md-3">{twitterExists() && <iframe title="Artist twitter" style={{ border: "none" }} width="500" height="800" data-tweet-url={artistData.strTwitter} src={twitSource}></iframe>}</div>
				</div>
			</div>
		</div>
	)
}

export default ArtistInstancePage
