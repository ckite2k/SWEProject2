import React, { useState, useEffect } from "react"
import "./Artists.css"
import NavBar from "../../NavBar"
import Filter from "../../Filter"
import BootstrapTable from "react-bootstrap-table-next"
import paginationFactory from "react-bootstrap-table2-paginator"
import { useHistory } from "react-router-dom"
import "react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css"
import "react-bootstrap-table2-filter/dist/react-bootstrap-table2-filter.min.css"
import ToolkitProvider from "react-bootstrap-table2-toolkit"
import filterFactory from "react-bootstrap-table2-filter"
import { Button } from "react-bootstrap"
import { Select, Slider } from "antd"
const { Option } = Select

function ArtistsPage() {
	const [artists, setArtists] = useState([])
	const [genres, setGenres] = useState([])
	const [popularity, setPopularity] = useState([])
	const [intFormedYear, setIntFormedYear] = useState([])
	const [intMembers, setIntMembers] = useState([])
	const [sortBy, setSortBy] = useState("name")
	const [sortDir, setSortDir] = useState(0)
	const [compare1, setCompare1] = useState("")
	const [compare1ID, setCompare1ID] = useState(0)
	const [compare2, setCompare2] = useState("")
	const [compare2ID, setCompare2ID] = useState(0)

	const columns = [
		{ dataField: "name", text: "Artist Name"},
		{ dataField: "strGenre", text: "Genre(s)"},
		{ dataField: "popularity", text: "Popularity" },
		{ dataField: "intFormedYear", text: "Year Formed" },
		{ dataField: "intMembers", text: "# of Members" },
	]

	const pagination = paginationFactory({
		alwaysShowAllBtns: true,
		showTotal: true,
	})

	const history = useHistory()

	const selectRow = {
		mode: 'checkbox',
		onSelect: (row, isSelect, rowIndex, e) => {
			console.log(isSelect)
			console.log(row)
			console.log(rowIndex)
			console.log(row.keyField)
			if(compare1 === row.name)
			{
				setCompare1("")
				setCompare1ID("");
			}
			else if(compare2 === row.name)
			{
				setCompare2("")
				setCompare2ID("");
				document.getElementById("CompareButton").style.visibility = "hidden"
			}
			else if(compare1 !== "" && compare2 !== "")
				return false
			else if(compare1 === "")
			{
				setCompare1(row.name)
				setCompare1ID(row.artistID);
			}
			else if(compare2 === "")
			{
				setCompare2(row.name)
				setCompare2ID(row.artistID);
				document.getElementById("CompareButton").style.visibility = "visible"
			}
		},
		hideSelectAll: true,
	}

	const rowEvents = {
		onClick: (e, row, rowIndex) => {
			history.push(`/artist/${row.artistID}`)
		},
	}

	useEffect(() => {
		const getArtistsData = async () => {
			try {
				const requestOptions = {
					method: "GET",
					headers: {},
				}

				var link = `https://spotifynder.com/api/artists?`
				link += "sort="
				link += sortBy
				link += "&direction="
				link += sortDir
				if (genres.length !== 0) {
					genres.forEach(function (a) {
						link += "&strGenre="
						link += a
					})
				}
				if (popularity.length !== 0) {
					popularity.forEach(function (g) {
						link += "&popularity="
						link += g
					})
				}
				if (intFormedYear.length !== 0) {
					intFormedYear.forEach(function (a) {
						link += "&intFormedYear="
						link += a
					})
				}
				if (intMembers.length !== 0) {
					intMembers.forEach(function (a) {
						link += "&intMembers="
						link += a
					})
				}
				console.log(link)
				const response = await fetch(link, requestOptions)
				const data = await response.json()
				let artistArray = data.message
				for (let artist of artistArray) {
					if (artist.intFormedYear + "" === "-1" || artist.intFormedYear + "" === "0") {
						artist.intFormedYear = "Unknown"
						artist.strDisbanded = "Unknown"
					}
					if (artist.intMembers + "" === "-1") {
						artist.intMembers = "Unknown"
					}
					if (artist.strGenre === "") {
						artist.strGenre = "Unknown"
					}
				}
				setArtists(artistArray)
			} catch (e) {
				console.error(e)
			}
		}
		getArtistsData()
	}, [genres, popularity, intFormedYear, intMembers, sortBy, sortDir])

	return (
		<div>
			<div>
				<NavBar />
			</div>
			<div style={{ textAlign: "right" }}>
				<Button variant="info" href="/artists/search" style={{ marginRight: 10, marginTop: 10 }}>
					Find Artists by Name
				</Button>
			</div>
			<div style={{ textAlign: "center" }}>
				<h1 style={{ justifyContent: "center" }}>Artists</h1>
			</div>

			<div class="filter-bar">
				<Filter name={"Genre"} onChange={setGenres} style={{ width: "100%" }}></Filter>
				<Select onSelect={setSortBy} defaultValue="name" style={{ width: "10%" }}>
					<Option value="name">Name</Option>
					<Option value="strGenre">Genre</Option>
					<Option value="popularity">Popularity</Option>
					<Option value="intFormedYear">Year Formed</Option>
					<Option value="intMembers">Members</Option>
				</Select>
				<Select onSelect={setSortDir} defaultValue="0" style={{ width: "10%" }}>
					<Option value="0">Ascending</Option>
					<Option value="1">Descending</Option>
				</Select>
			</div>

			<div class="filter-bar">
				<div class="filter-box-2">
					<span style={{ display: "inline-block" }}>Popularity</span>
					<Slider range defaultValue={[0, 100]} onAfterChange={setPopularity} />
				</div>
				<div class="filter-box-2">
					<span style={{ display: "inline-block" }}>Year Formed</span>
					<Slider range defaultValue={[1900, 2021]} min={1900} max={2021} onAfterChange={setIntFormedYear} />
				</div>
				<div class="filter-box-2">
					<span style={{ display: "inline-block" }}>Number of Members</span>
					<Slider range defaultValue={[0, 10]} max={10} onAfterChange={setIntMembers} />
				</div>
			</div>

			<div style={{ margin: "auto", textAlign: "center", fontFamily: "Arial, Helveticca, sans-serif"}}>
				<h4 style={{marginBottom: 5}}>Select two artists from the table to compare.</h4>
				<h4>Comparing {compare1 + (compare1 !== "" && compare2 !== "" ? " and " : "") + compare2}</h4>
				<Button id="CompareButton" href={`/artists/compare/${compare1ID}/${compare2ID}`} style={{ visibility: "hidden", marginRight: 2, marginTop: 2 }}>Go!</Button>
			</div>

			<div style={{ padding: "10px", float: "right" }}>
				<ToolkitProvider
					keyField="artistID"
					data={artists}
					columns={columns}
				>
					{(props) => (
						<div style={{ padding: "10px", float: "right" }}>
							<BootstrapTable filter={filterFactory()} rowEvents={rowEvents} hover={true} selectRow={selectRow} rowStyle={{ cursor: "pointer" }} pagination={pagination} {...props.baseProps} />
						</div>
					)}
				</ToolkitProvider>
				
			</div>
		</div>
	)
}

export default ArtistsPage
