import React, { useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import NavBar from "../../NavBar"
import Image from "react-bootstrap/Image"
const https = require("https")
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"

function SongComparePage() {
	const { song1ID, song2ID } = useParams()
	const [song1Data, setSong1Data] = useState({})
	const [song2Data, setSong2Data] = useState({})

	const getSongData = async () => {
		try {
			const httpsAgent = new https.Agent({
				rejectUnauthorized: false,
			})
			const requestOptions = {
				method: "GET",
				agent: httpsAgent,
				headers: {},
			}
			const response1 = await fetch(`https://spotifynder.com/api/songs?songID=${song1ID}`, requestOptions)
			const data1 = await response1.json()
			setSong1Data(data1.message[0])
            
			const response2 = await fetch(`https://spotifynder.com/api/songs?songID=${song2ID}`, requestOptions)
			const data2 = await response2.json()
			setSong2Data(data2.message[0])
		} catch (e) {
			console.error(e)
		}
	}

	useEffect(() => {
		getSongData()
	})

	return (
		<div>
			<div>
				<NavBar />
			</div>

			<a href="/songs" class="btn btn-primary" style={{ marginTop: "15px", marginLeft: "15px" }}>
				Back to Songs
			</a>
			<div style={{ margin: "auto", marginTop: "50px", width: "80%" }}>
				<div class="jumbotron">
					<h1 class="display-1" style={{ display: "inline-block", width: "70%" }}>
						{song1Data.name}
					</h1>
					<Image src={song1Data.image} alt={song1Data.album} fluid style={{ float: "right", width: 300 }} />
					<h1 class="display-4" style={{ width: "70%" }}>
						Artist:{" "}
						<a href={"/artist/" + song1Data.artistID} id="track-artist">
							{song1Data.artist}
						</a>
					</h1>
					<h1 class="display-4" style={{ width: "70%" }}>
						Album:{" "}
						<a href={"/album/" + song1Data.albumID} id="track-album">
							{song1Data.album}
						</a>
					</h1>
					<hr class="my-4"></hr>
					<h3 style={{ display: "inline-block" }}>Genre: {song1Data.genres}</h3>
					<iframe title="Song 1 preview" src={"https://open.spotify.com/embed/track/" + song1Data.songID} width="250" height="80" frameborder="0" allowtransparency="true" allow="encrypted-media" style={{ float: "right" }}></iframe>
					<h3>Acousticness: {round(song1Data.acousticness * 100)}%</h3>
					<h3>Danceability: {round(song1Data.danceability * 100)}%</h3>
					<h3>Energy: {round(song1Data.energy * 100)}%</h3>
					<h3>Liveness: {round(song1Data.liveness * 100)}%</h3>
					<h3>Instrumental: {song1Data.instrumentalness > 0.5 ? "Yes" : "No"}</h3>
				</div>
			</div>

            <div style={{ margin: "auto", marginTop: "50px", width: "80%" }}>
				<div class="jumbotron">
					<h1 class="display-1" style={{ display: "inline-block", width: "70%" }}>
						{song2Data.name}
					</h1>
					<Image src={song2Data.image} alt={song2Data.album} fluid style={{ float: "right", width: 300 }} />
					<h1 class="display-4" style={{ width: "70%" }}>
						Artist:{" "}
						<a href={"/artist/" + song2Data.artistID} id="track-artist">
							{song2Data.artist}
						</a>
					</h1>
					<h1 class="display-4" style={{ width: "70%" }}>
						Album:{" "}
						<a href={"/album/" + song2Data.albumID} id="track-album">
							{song2Data.album}
						</a>
					</h1>
					<hr class="my-4"></hr>
					<h3 style={{ display: "inline-block" }}>Genre: {song2Data.genres}</h3>
					<iframe title="Song 2 preview" src={"https://open.spotify.com/embed/track/" + song2Data.songID} width="250" height="80" frameborder="0" allowtransparency="true" allow="encrypted-media" style={{ float: "right" }}></iframe>
					<h3>Acousticness: {round(song2Data.acousticness * 100)}%</h3>
					<h3>Danceability: {round(song2Data.danceability * 100)}%</h3>
					<h3>Energy: {round(song2Data.energy * 100)}%</h3>
					<h3>Liveness: {round(song2Data.liveness * 100)}%</h3>
					<h3>Instrumental: {song2Data.instrumentalness > 0.5 ? "Yes" : "No"}</h3>
				</div>
			</div>
            <br/>
		</div>
	)
}

/**
 * Rounds a number to one decimal place.
 * @param {A string to round} num
 * @returns the rounded number.
 */
function round(num) {
	return parseFloat(num).toFixed(1)
}

export default SongComparePage
