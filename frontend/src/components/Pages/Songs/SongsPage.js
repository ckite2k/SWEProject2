import React, { useState, useEffect } from "react"
import NavBar from "../../NavBar"
import Filter from "../../Filter"
import BootstrapTable from "react-bootstrap-table-next"
import paginationFactory from "react-bootstrap-table2-paginator"
import { useHistory } from "react-router-dom"
import "react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css"
import "react-bootstrap-table2-filter/dist/react-bootstrap-table2-filter.min.css"
import ToolkitProvider from "react-bootstrap-table2-toolkit"
import filterFactory from "react-bootstrap-table2-filter"
import { Select, Slider } from "antd"
import { Button } from "react-bootstrap"
const { Option } = Select

function SongsPage() {
	const [songs, setSongs] = useState([])
	const [artist, setArtist] = useState([])
	const [genres, setGenres] = useState([])
	const [popularity, setPopularity] = useState([])
	const [duration, setDuration] = useState([])
	const [sortBy, setSortBy] = useState("name")
	const [sortDir, setSortDir] = useState(0)
	const [compare1, setCompare1] = useState("")
	const [compare1ID, setCompare1ID] = useState(0)
	const [compare2, setCompare2] = useState("")
	const [compare2ID, setCompare2ID] = useState(0)

	function secToMinSec(seconds) {
		var minutes = Math.floor(seconds / 60)
		return `${minutes}:${seconds % 60}${seconds < 10 ? "0" : ""}`
	}

	function millisToMinutesAndSeconds(millis) {
		var minutes = Math.floor(millis / 60000)
		var seconds = ((millis % 60000) / 1000).toFixed(0)
		return minutes + ":" + (seconds < 10 ? "0" : "") + seconds
	}

	const columns = [
		{ dataField: "name", text: "Song Name" },
		{ dataField: "artist", text: "Artist" },
		{ dataField: "genres", text: "Genre(s)" },
		{ dataField: "popularity", text: "Popularity" },
		{ dataField: "duration_ms", text: "Duration" },
	]

	const pagination = paginationFactory({
		alwaysShowAllBtns: true,
		showTotal: true,
	})

	const history = useHistory()

	const selectRow = {
		mode: 'checkbox',
		onSelect: (row, isSelect, rowIndex, e) => {
			console.log(isSelect)
			console.log(row)
			console.log(rowIndex)
			console.log(row.keyField)
			if(compare1 === row.name)
			{
				setCompare1("")
				setCompare1ID("");
			}
			else if(compare2 === row.name)
			{
				setCompare2("")
				setCompare2ID("");
				document.getElementById("CompareButton").style.visibility = "hidden"
			}
			else if(compare1 !== "" && compare2 !== "")
				return false
			else if(compare1 === "")
			{
				setCompare1(row.name)
				setCompare1ID(row.songID);
			}
			else if(compare2 === "")
			{
				setCompare2(row.name)
				setCompare2ID(row.songID);
				document.getElementById("CompareButton").style.visibility = "visible"
			}
		},
		hideSelectAll: true,
	}

	const rowEvents = {
		onClick: (e, row, rowIndex) => {
			history.push(`/song/${row.songID}`)
		},
	}

	useEffect(() => {
		const getSongData = async () => {
			try {
				const requestOptions = {
					method: "GET",
					headers: {},
				}
				var link = `https://spotifynder.com/api/songs?`
				link += "sort="
				link += sortBy
				link += "&direction="
				link += sortDir
				if (artist.length !== 0) {
					artist.forEach(function (a) {
						link += "&artist="
						link += a
					})
				}
				if (genres.length !== 0) {
					genres.forEach(function (g) {
						link += "&genres="
						link += g
					})
				}
				if (popularity.length !== 0) {
					popularity.forEach(function (a) {
						link += "&popularity="
						link += a
					})
				}
				if (duration.length !== 0) {
					duration.forEach(function (a) {
						link += "&duration_ms="
						link += a * 1000
					})
				}
				const response = await fetch(link, requestOptions)
				const data = await response.json()

				let songArray = data.message
				for (let song of songArray) {
					song.duration_ms = millisToMinutesAndSeconds(song.duration_ms)
				}

				setSongs(data.message)
			} catch (e) {
				console.error(e)
			}
		}
		getSongData()
	}, [artist, genres, popularity, duration, sortBy, sortDir])

	return (
		<div>
			<div>
				<NavBar />
			</div>
			<div style={{ textAlign: "right" }}>
				<Button variant="info" href="/songs/search" style={{ marginRight: 10, marginTop: 10 }}>
					Find Songs by Name
				</Button>
			</div>
			<div style={{ textAlign: "center" }}>
				<h1 style={{ justifyContent: "center" }}>Songs</h1>
			</div>

			<div class="filter-bar">
				<Filter name={"Artist"} onChange={setArtist} style={{ width: "100%" }}></Filter>
				<Filter name={"Genre"} onChange={setGenres} style={{ width: "100%" }}></Filter>

				<Select onSelect={setSortBy} defaultValue="name" style={{ width: "10%" }}>
					<Option value="name">Name</Option>
					<Option value="artist">Artist</Option>
					<Option value="genres">Genres</Option>
					<Option value="popularity">Popularity</Option>
					<Option value="duration_ms">Song Duration</Option>
				</Select>
				<Select onSelect={setSortDir} defaultValue="0" style={{ width: "10%" }}>
					<Option value="0">Ascending</Option>
					<Option value="1">Descending</Option>
				</Select>
			</div>

			<div class="filter-bar">
				<div class="filter-box-2">
					<span style={{ display: "inline-block" }}>Popularity</span>
					<Slider range defaultValue={[0, 100]} onAfterChange={setPopularity} />
				</div>
				<div class="filter-box-2">
					<span style={{ display: "inline-block" }}>Song Duration</span>
					<Slider tipFormatter={secToMinSec} range defaultValue={[0, 1300]} max="1300" onAfterChange={setDuration} />
				</div>
			</div>

			<div style={{ margin: "auto", textAlign: "center", fontFamily: "Arial, Helveticca, sans-serif"}}>
				<h4 style={{marginBottom: 5}}>Select two songs from the table to compare.</h4>
				<h4>Comparing {compare1 + (compare1 !== "" && compare2 !== "" ? " and " : "") + compare2}</h4>
				<Button id="CompareButton" href={`/songs/compare/${compare1ID}/${compare2ID}`} style={{ visibility: "hidden", marginRight: 2, marginTop: 2 }}>Go!</Button>
			</div>

			<br />
			<div style={{ padding: "10px" }}>
				<ToolkitProvider
					keyField="songID"
					data={songs}
					columns={columns}
				>
					{(props) => (
						<div>
							<BootstrapTable filter={filterFactory()} hover={true} rowStyle={{ cursor: "pointer" }} selectRow={selectRow} rowEvents={rowEvents} pagination={pagination} {...props.baseProps} />
						</div>
					)}
				</ToolkitProvider>
			</div>
		</div>
	)
}

export default SongsPage