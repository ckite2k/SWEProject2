import React, { Component } from "react";
import {
    Scatter,
    ScatterChart,
    XAxis,
    YAxis,
    ZAxis,
    CartesianGrid,
    Tooltip
} from "recharts";

class V4 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: null
        }
    }

    async componentDidMount() {
        const requestOptions = {
            method: "GET",
            headers: {},
        }
        const response = await fetch(`https://readyrecipes.me/api/geolocation`, requestOptions)
        const data = await response.json()
        this.setState({
            data: data.data
        })
    }

    getValues() {
        var values = [];
        for (var i = 0; i < this.state.data.length; i+= 10) {
            var item = this.state.data[i];
            var value = {"lattitude" : item['lat'], "longitude" : item['lon'], "name" : item['name']};
            values.push(value);
        }

        return values;
    }

    render() {
        let data = this.state.data;

        return (data != null) ? (
            <div>
                <h3 className="text-center" style={{ marginTop: '30px'}}> Lattitude and Longitude of ReadyRecipes Geolocations </h3>
                <div style={{ display: "flex", alignItems: "center", justifyContent: "center"}} >
                    <div style={{ backgroundColor: "#1072e3" }}>
                        <ScatterChart
                            width={1000}
                            height={500}
                            data={this.getValues()}
                            margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                            <CartesianGrid />
                            <XAxis type="number" dataKey="longitude" stroke="#ffffff"/>
                            <YAxis type="number" dataKey="lattitude" stroke="#ffffff" />
                            <ZAxis type="string" dataKey="name" />
                            <Tooltip cursor={{ strokeDasharray: '3 3' }} />
                            <Scatter name="Plot" data={this.getValues()} fill="#1DB954"/>
                        </ScatterChart>
                    </div>
                </div>
            </div>
        ) : (
            <div>
                <h3 className="text-center"> Lattitude and Longitude of ReadyRecipes Geolocations </h3>
                <p className="text-center"> Loading Data...</p>
            </div>
        );
    }
}

export default V4;
