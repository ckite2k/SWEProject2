import React, { Component } from "react";
import {
    Scatter,
    ScatterChart,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip
} from "recharts";

class SongsChart extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: null
        }
    }

    async componentDidMount() {
        const requestOptions = {
            method: "GET",
            headers: {},
        }

        var link = `https://spotifynder.com/api/albums`
        const response = await fetch(link, requestOptions)
        const data = await response.json()
        this.setState({
            data: data.message
        })
        console.log(data.message)
    }

    getValues() {
        var values = [];

        for (var i = 0; i < this.state.data.length; i++) {
            var item = this.state.data[i];
            if(this.state.data[i].totalTracks <= 40)
            {
                var value = {"popularity" : item['popularity'], "totalTracks" : item['totalTracks']};
                values.push(value);
            }
        }

        return values;
    }

    render() {
        let data = this.state.data;

        return (data != null) ? (
            <div>
                <h3 className="text-center" style={{ marginTop: '30px'}}> Album Popularity Related to Total Tracks </h3>
                <p className="text-center"> (x - Album Total Tracks, y - Album Popularity)</p>
                <div style={{ display: "flex", alignItems: "center", justifyContent: "center"}} >
                    <ScatterChart
                        width={1200}
                        height={500}
                        data={this.getValues()}
                        margin={{ top: 5, right: 0, left: 140, bottom: 40 }}>
                        <CartesianGrid />
                        <YAxis type="number" dataKey="popularity" label={{ value: 'Album Popularity', position: 'left' }} />
                        <XAxis type="number" dataKey="totalTracks" label={{ value: 'Total Tracks', position: 'bottom' }} />
                        <Tooltip cursor={{ strokeDasharray: '3 3' }} />
                        <Scatter name="Plot" data={this.getValues()} fill="#C1A954" />
                    </ScatterChart>
                </div>
            </div>
        ) : (
            <div>
                <h3 className="text-center"> Album Popularity Related to Total Tracks </h3>
                <p className="text-center"> Loading Data...</p>
            </div>
        );
    }
}

export default SongsChart;
