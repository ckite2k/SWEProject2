import React, { useState, useEffect} from "react";
import 'antd/dist/antd.css';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';

function V5() {

  const [discoverData, setDiscoverData] = useState([])
  const getDiscoverData = async () => {
		try {
			const requestOptions = {
				method: "GET",
				headers: {},
			}
			const response = await fetch(`https://readyrecipes.me/api/gardencrops`, requestOptions)
			const data = await response.json()
			let crops = data.data
      console.log(crops)
      crops = crops.filter(crop => crop.year != null).map(crop => ({
        name: crop.common_name,
        year: crop.year
      }))
    let discData = []
     for(let i = 1750; i < 2020; i += 10) {
        discData.push({ 
          year: i,
          Crops: crops.filter(crop => crop.year >= i && crop.year < i + 10).length
        })
     }

     //console.log(discData)
     setDiscoverData(discData)
		} catch (e) {
			console.error(e)
		}
	}

  useEffect( () => {
    getDiscoverData()
  }, [])


  
    return (discoverData.length !== 0) ? (
      <div>
      <h3 className="text-center" style={{ marginTop: '30px'}}> Crops Classified Each Decade </h3>
      <div style={{ display: "flex", alignItems: "center", justifyContent: "center"}} >
          <div>
          <BarChart
            width={900}
            height={400}
            data={discoverData}
            margin={{
              top: 5,
              right: 30,
              left: 20,
              bottom: 5,
            }}
          >
            <CartesianGrid strokeDasharray="2 2" />
            <XAxis dataKey="year" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Bar dataKey="Crops" fill="#7851A9" />
          </BarChart>
          </div>
      </div>
      </div>
  ) : (
      <div>
          <h3 className="text-center"> Crops Classified Each Decade </h3>
          <p className="text-center"> Loading Data...</p>
      </div>
  );
}

export default V5;