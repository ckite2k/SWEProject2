import React from "react"
import { useState } from "react"
import Navbar from "react-bootstrap/Navbar"
import Nav from "react-bootstrap/Nav"
import Form from "react-bootstrap/Form"
import FormControl from "react-bootstrap/FormControl"
import Button from "react-bootstrap/Button"
import logo from "./Images/logo192.png"

function NavBar() {
	const [query, setQuery] = useState("")

	return (
		<div>
			<Navbar bg="primary" variant="dark" sticky="top">
				<Navbar.Brand href="/">
					<img src={logo} width="30" height="30" hspace="10" className="d-inline-block align-top" alt="Spotifynder Logo" />
					Spotifynder
				</Navbar.Brand>
				<Nav className="mr-auto">
					<Nav.Link id="home_link" href="/">
						Home
					</Nav.Link>
					<Nav.Link id="songs_link" href="/songs">
						Songs
					</Nav.Link>
					<Nav.Link id="artists_link" href="/artists">
						Artists
					</Nav.Link>
					<Nav.Link id="albums_link" href="/albums">
						Albums
					</Nav.Link>
					<Nav.Link id="about_link" href="/about">
						About
					</Nav.Link>
					<Nav.Link id="vis_link" href="/visualizations">
						Visualizations
					</Nav.Link>
				</Nav>
				<Form inline>
					<FormControl
						onChange={(event) => {
							setQuery(event.target.value)
						}}
						type="text"
						placeholder="Search"
						className="mr-auto"
					/>
					<Button variant="info" href={"/search/" + query}>
						Search
					</Button>
				</Form>
			</Navbar>
		</div>
	)
}

export default NavBar
