from flask import Flask, request, make_response, send_from_directory, render_template, Blueprint
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS



app = Flask(__name__)
CORS(app)
api = Blueprint('api', __name__)
PASSWORD = "qazxcwsde123"
PUBLIC_IP_ADDRESS = "104.198.196.255"
DBNAME = "SpotifynderDB"
PROJECT_ID = "genial-analyzer-306023"
INSTANCE_NAME = "musicfinderdatabase"

app.config["SECRET_KEY"] = "yoursecretkey"
app.config["SQLALCHEMY_DATABASE_URI"] = f"mysql+mysqldb://root:{PASSWORD}@{PUBLIC_IP_ADDRESS}/{DBNAME}?unix_socket=/cloudsql/{PROJECT_ID}:{INSTANCE_NAME}"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"]= True


db = SQLAlchemy(app)

class Songs(db.Model):
    name = db.Column(db.String(100)) 
    nameID = db.Column(db.String(100), primary_key = True) 
    artist = db.Column(db.String(100)) 
    artistID = db.Column(db.String(100))
    album = db.Column(db.String(100)) 
    albumID = db.Column(db.String(100))
    genres = db.Column(db.String(100))
    popularity = db.Column(db.Integer)
    duration_ms = db.Column(db.Integer)
    acousticness = db.Column(db.Float)
    danceability = db.Column(db.Float)
    energy = db.Column(db.Float)
    liveness = db.Column(db.Float)
    instrumentalness = db.Column(db.Float)
    image = db.Column(db.String(100))
    
class Artists(db.Model):
    name = db.Column(db.String(100))
    artistID = db.Column(db.String(100), primary_key = True)
    followers = db.Column(db.Integer)
    popularity = db.Column(db.Integer)
    topTrack = db.Column(db.String(100)) 
    topID = db.Column(db.String(100))
    topAlbum = db.Column(db.String(100)) 
    topAlbumID = db.Column(db.String(100))
    strGenre = db.Column(db.String(100)) 
    intFormedYear = db.Column(db.Integer)
    strDisbanded = db.Column(db.String(100)) 
    strCountry = db.Column(db.String(100)) 
    intMembers = db.Column(db.Integer)
    strArtistThumb = db.Column(db.String(100)) 
    strTwitter = db.Column(db.String(100))

class Albums(db.Model):
    name = db.Column(db.String(100))
    albumID = db.Column(db.String(100), primary_key = True)
    artist = db.Column(db.String(100))
    artistID = db.Column(db.String(100))
    genres = db.Column(db.String(100))
    popularity = db.Column(db.Integer)
    totalTracks = db.Column(db.Integer)
    releaseDate = db.Column(db.String(100))
    label = db.Column(db.String(100))
    albumImage = db.Column(db.String(100)) 
    recommendedSong = db.Column(db.String(100)) 
    recommendedSongID = db.Column(db.String(100)) 