from flask import Flask, request, make_response, send_from_directory, render_template, Blueprint
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_
from flask_cors import CORS
from models import Songs, Artists, Albums, app, db

def query_safe(key, queryDict):
    try:
        return queryDict[key]
    except:
        return None

def get_album_json(album):
    return {
        "name" : album.name,
        "albumID" : album.albumID,
        "artist" : album.artist, 
        "artistID" : album.artistID,
        "genres" : album.genres,
        "popularity" : album.popularity,
        "totalTracks" : album.totalTracks,
        "releaseDate" : album.releaseDate,
        "label" : album.label,
        "albumImage" : album.albumImage,
        "recommendedSong" : album.recommendedSong,
        "recommendedSongID" : album.recommendedSongID
    }


def filter_albums(query, args):
    artist = query_safe("artist", args)
    genres = query_safe("genres", args)
    popularity = query_safe("popularity", args)
    totalTracks = query_safe("totalTracks", args)

    if artist:
        query = query.filter(Albums.artist.in_(artist))

    if genres:
        query = query.filter(Albums.genres.in_(genres))

    if popularity:
        query = query.filter(Albums.popularity >= (popularity[0]))
        query = query.filter(Albums.popularity <= (popularity[1]))

    if totalTracks:
        query = query.filter(Albums.totalTracks >= (totalTracks[0]))
        query = query.filter(Albums.totalTracks <= (totalTracks[1]))

    return query

def sort_albums(query, sort, direction):
    toSort = Albums.name
    if sort == "name":
        toSort = Albums.name
    elif sort == "artist":
        toSort = Albums.artist
    elif sort == "genres":
        toSort = Albums.genres
    elif sort == "popularity":
        toSort = Albums.popularity
    elif sort == "totalTracks":
        toSort = Albums.totalTracks
    elif sort == "releaseDate":
        toSort = Albums.releaseDate
    elif sort == "label":
        toSort = Albums.label
    
    if direction:
        return query.order_by(toSort.desc())
    else:
        return query.order_by(toSort.asc())



def search_albums(search, query_results):
    if search is None or len(search) < 1:
        return query_results
    search = search[0].strip()
    keywords = search.split()

    results = []
    for word in keywords:
        # contains might not work ?
        results.append(Albums.name.like("%"+word+"%"))
        results.append(Albums.artist.like("%"+word+"%"))
        results.append(Albums.label.like("%"+word+"%"))
        results.append(Albums.genres.like("%"+word+"%"))
    query_results = query_results.filter(or_(*tuple(results)))

    return query_results 
