from flask import Flask, request, make_response, send_from_directory, render_template, Blueprint
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from models import Songs, Artists, Albums, app, db, api
from Songs import *
from Artists import *
from Albums import *


def query_safe(key, queryDict):
    try:
        return queryDict[key]
    except:
        return None

def query_safe_int(key, queryDict):
    try:
        return int(queryDict[key][0])
    except:
        return None

def query_safe_str(key, queryDict):
    try:
        return queryDict[key][0]
    except:
        return None

# If a nameID is not specified, gives a list of json(?) objects for each song in DB
# If a nameID is specified, return exactly one json(?) object for that specific song
@app.route('/api/songs', methods=["GET"])
def songs_view(): 
    args = request.args.to_dict(flat=False)
    songsQuery = Songs.query

    songID = query_safe_str("songID", args)
    if songID is not None:
        song = songsQuery.filter(Songs.nameID == songID).all()
        response = list()
        try:
            response.append(get_song_json(song[0]))
        except:
            pass
        status = 'success' if len(response) else 'failure'
        return make_response({ 
            'status' : status, 
            'message': response,
            'count' : 1
        }, 200 if status == 'success' else 400)

    search = query_safe("search", args)
    songsQuery = search_songs(search, songsQuery)

    songsQuery = filter_songs(songsQuery, args)

    sort = query_safe_str("sort", args)
    direction = query_safe_int("direction", args)
    songsQuery = sort_songs(songsQuery, sort, direction)

    songs = songsQuery.all()

    page = query_safe_int("page", args)
    if page is None:
        page = 0
    perPage = query_safe_int("perPage", args)
    if perPage is None:
        perPage = 0

    response = list()
    i = 0
    for song in songs:
        if page != 0 and perPage != 0:
            if i >= ((page - 1) * perPage) and i < (page * perPage):
                response.append(get_song_json(song))
        else :
            response.append(get_song_json(song))
        i += 1

    status = 'success' if len(response) else 'failure'
    return make_response({ 
        'status' : status, 
        'message': response,
        'count' : i
    }, 200 if status == 'success' else 400) 
      

@app.route('/api/artists', methods=["GET"])
def artist_view(): 
    args = request.args.to_dict(flat=False)
    artistQuery = Artists.query

    artistID = query_safe_str("artistID", args)
    if artistID is not None:
        artist = artistQuery.filter_by(artistID=artistID).all()
        response = list()
        try:
            response.append(get_artist_json(artist[0]))
        except:
            pass
        status = 'success' if len(response) else 'failure'
        return make_response({ 
            'status' : status, 
            'message': response,
            'count' : 1
        }, 200 if status == 'success' else 400)

    search = query_safe("search", args)
    artistQuery = search_artists(search, artistQuery)

    artistQuery = filter_artists(artistQuery, args)

    sort = query_safe_str("sort", args)
    direction = query_safe_int("direction", args)
    artistQuery = sort_artists(artistQuery, sort, direction)

    artists = artistQuery.all()

    page = query_safe_int("page", args)
    if page is None:
        page = 0
    perPage = query_safe_int("perPage", args)
    if perPage is None:
        perPage = 0

    response = list()
    i = 0
    for artist in artists:
        if page != 0 and perPage != 0:
            if i >= ((page - 1) * perPage) and i < (page * perPage):
                response.append(get_artist_json(artist))
        else :
            response.append(get_artist_json(artist))
        i += 1

    status = 'success' if len(response) else 'failure'
    return make_response({ 
        'status' : status, 
        'message': response,
        'count' : i
    }, 200 if status == 'success' else 400)


@app.route('/api/albums', methods=["GET"])
def album_view(): 
    args = request.args.to_dict(flat=False)
    albumQuery = Albums.query

    albumID = query_safe_str("albumID", args)
    if albumID is not None:
        album = albumQuery.filter_by(albumID=albumID).all()
        response = list()
        try:
            response.append(get_album_json(album[0]))
        except:
            pass
        status = 'success' if len(response) else 'failure'
        return make_response({ 
            'status' : status, 
            'message': response,
            'count' : 1
        }, 200 if status == 'success' else 400)

    search = query_safe("search", args)
    albumQuery = search_albums(search, albumQuery)

    albumQuery = filter_albums(albumQuery, args)

    sort = query_safe_str("sort", args)
    direction = query_safe_int("direction", args)
    albumQuery = sort_albums(albumQuery, sort, direction)

    albums = albumQuery.all()

    page = query_safe_int("page", args)
    if page is None:
        page = 0
    perPage = query_safe_int("perPage", args)
    if perPage is None:
        perPage = 0

    response = list()
    i = 0
    for album in albums:
        if page != 0 and perPage != 0:
            if i >= ((page - 1) * perPage) and i < (page * perPage):
                response.append(get_album_json(album))
        else :
            response.append(get_album_json(album))
        i += 1

    status = 'success' if len(response) else 'failure'
    return make_response({ 
        'status' : status, 
        'message': response,
        'count' : i
    }, 200 if status == 'success' else 400)


app.register_blueprint(api, url_prefix='/api')

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port=5050)

